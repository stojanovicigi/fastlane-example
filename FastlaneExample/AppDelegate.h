//
//  AppDelegate.h
//  FastlaneExample
//
//  Created by Igor Stojanovic on 3/9/15.
//  Copyright (c) 2015 Codecentric doo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

